class TripReportsController < ApplicationController
  before_action :set_trip_report, only: [:show, :edit, :update, :destroy]

  # GET /trip_reports
  # GET /trip_reports.json
  def index
    @trip_reports = TripReport.all
  end

  # GET /trip_reports/1
  # GET /trip_reports/1.json
  def show
  end

  # GET /trip_reports/new
  def new
    @trip_report = TripReport.new
    @patient = Patient.new
  end

  # GET /trip_reports/1/edit
  def edit
  end

  # POST /trip_reports
  # POST /trip_reports.json
  def create
    @trip_report = TripReport.new(trip_report_params)

    respond_to do |format|
      if @trip_report.save
        format.html { redirect_to @trip_report, notice: 'Trip report was successfully created.' }
        format.json { render action: 'show', status: :created, location: @trip_report }
      else
        format.html { render action: 'new' }
        format.json { render json: @trip_report.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /trip_reports/1
  # PATCH/PUT /trip_reports/1.json
  def update
    respond_to do |format|
      if @trip_report.update(trip_report_params)
        format.html { redirect_to @trip_report, notice: 'Trip report was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @trip_report.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /trip_reports/1
  # DELETE /trip_reports/1.json
  def destroy
    @trip_report.destroy
    respond_to do |format|
      format.html { redirect_to trip_reports_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trip_report
      @trip_report = TripReport.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trip_report_params
      params.require(:trip_report).permit(:run_number)
    end
end
