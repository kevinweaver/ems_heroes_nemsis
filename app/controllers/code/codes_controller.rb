class Code::CodesController < ApplicationController
  before_action :set_code_code, only: [:show, :edit, :update, :destroy]

  # GET /code/codes
  # GET /code/codes.json
  def index
    @code_codes = Code::Code.all
  end

  # GET /code/codes/1
  # GET /code/codes/1.json
  def show
  end

  # GET /code/codes/new
  def new
    @code_code = Code::Code.new
  end

  # GET /code/codes/1/edit
  def edit
  end

  # POST /code/codes
  # POST /code/codes.json
  def create
    @code_code = Code::Code.new(code_code_params)

    respond_to do |format|
      if @code_code.save
        format.html { redirect_to @code_code, notice: 'Code was successfully created.' }
        format.json { render action: 'show', status: :created, location: @code_code }
      else
        format.html { render action: 'new' }
        format.json { render json: @code_code.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /code/codes/1
  # PATCH/PUT /code/codes/1.json
  def update
    respond_to do |format|
      if @code_code.update(code_code_params)
        format.html { redirect_to @code_code, notice: 'Code was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @code_code.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /code/codes/1
  # DELETE /code/codes/1.json
  def destroy
    @code_code.destroy
    respond_to do |format|
      format.html { redirect_to code_codes_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_code_code
      @code_code = Code::Code.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def code_code_params
      params.require(:code_code).permit(:reference, :description, :type)
    end
end
