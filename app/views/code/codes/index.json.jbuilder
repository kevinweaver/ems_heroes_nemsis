json.array!(@code_codes) do |code_code|
  json.extract! code_code, :reference, :description, :type
  json.url code_code_url(code_code, format: :json)
end