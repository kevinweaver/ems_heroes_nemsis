json.array!(@trip_reports) do |trip_report|
  json.extract! trip_report, :run_number
  json.url trip_report_url(trip_report, format: :json)
end