# == Schema Information
#
# Table name: code_codes
#
#  id          :integer          not null, primary key
#  reference   :string(255)
#  description :string(255)
#  type        :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#

class Code::NotValue < Code::Code

end
