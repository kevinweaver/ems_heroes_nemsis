# == Schema Information
#
# Table name: patient_patients
#
#  id                     :integer          not null, primary key
#  trip_report_id         :integer
#  first_name             :string(255)
#  first_name_type        :string(255)
#  last_name              :string(255)
#  last_name_type         :string(255)
#  middle_name            :string(255)
#  home_address           :string(255)
#  home_address2          :string(255)
#  home_city              :string(255)
#  home_county            :string(255)
#  home_state             :string(255)
#  home_zip_code          :string(255)
#  home_zip_code_type     :string(255)
#  home_country           :string(255)
#  home_census_tract      :string(255)
#  social_security_number :integer
#  gender                 :string(255)
#  gender_type            :string(255)
#  age                    :string(255)
#  age_type               :string(255)
#  age_units_type         :string(255)
#  age_units              :string(255)
#  date_of_birth_type     :string(255)
#  date_of_birth          :string(255)
#  drivers_license_state  :string(255)
#  drivers_license_number :string(255)
#  created_at             :datetime
#  updated_at             :datetime
#

class Patient::Patient < ActiveRecord::Base

  belongs_to :trip_report
  has_many :races, :class_name => "Patient::Race"
  has_many :phone_numbers, :class_name => "Patient::PhoneNumber"
  has_many :email_addresses, :class_name => "Patient::EmailAddress"

end
