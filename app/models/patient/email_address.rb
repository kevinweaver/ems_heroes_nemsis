# == Schema Information
#
# Table name: patient_email_addresses
#
#  id              :integer          not null, primary key
#  patient_id      :integer
#  email_code_type :string(255)
#  email_code_id   :integer
#  email           :string(255)
#  correlation_id  :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#

class Patient::EmailAddress < ActiveRecord::Base

  include Nemsis::Codeable
  belongs_to :patient
  has_nemsis_code :email

end
