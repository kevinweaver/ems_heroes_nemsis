# == Schema Information
#
# Table name: patient_phone_numbers
#
#  id              :integer          not null, primary key
#  patient_id      :integer
#  phone_code_type :string(255)
#  phone_code_id   :integer
#  phone           :string(255)
#  correlation_id  :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#

class Patient::PhoneNumber < ActiveRecord::Base

  include Nemsis::Codeable
  belongs_to :patient
  has_nemsis_code :phone

end
