# == Schema Information
#
# Table name: patient_races
#
#  id             :integer          not null, primary key
#  patient_id     :integer
#  created_at     :datetime
#  updated_at     :datetime
#  race_code_type :string(255)
#  race_code_id   :integer
#

class Patient::Race < ActiveRecord::Base

  include Nemsis::Codeable
  belongs_to :patient, :class_name => "Patient::Patient"
  has_nemsis_code :race

end
