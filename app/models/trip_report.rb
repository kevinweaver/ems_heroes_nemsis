# == Schema Information
#
# Table name: trip_reports
#
#  id         :integer          not null, primary key
#  run_number :integer
#  created_at :datetime
#  updated_at :datetime
#

class TripReport < ActiveRecord::Base
  
  has_one :patient, :class_name => "Patient::Patient"
  accepts_nested_attributes_for :patient

end
