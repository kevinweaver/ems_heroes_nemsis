# == Schema Information
#
# Table name: patient_races
#
#  id             :integer          not null, primary key
#  patient_id     :integer
#  created_at     :datetime
#  updated_at     :datetime
#  race_code_type :string(255)
#  race_code_id   :integer
#

require 'spec_helper'

describe Patient::Race do
  context "attributes" do

    before :each do
      @race = Patient::Race.new
      #Asian Code
      @race_code = Code::Race.find_by_reference("2514003")
    end

    it "has a race code" do
      @race.race_code = @race_code
      @race.save.should eq(true)
      race = Patient::Race.find(@race.id)
      race.race_code.description.should eq("Asian")
    end
  end
end
