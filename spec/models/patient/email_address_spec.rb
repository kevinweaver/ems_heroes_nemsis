# == Schema Information
#
# Table name: patient_email_addresses
#
#  id              :integer          not null, primary key
#  patient_id      :integer
#  email_code_type :string(255)
#  email_code_id   :integer
#  email           :string(255)
#  correlation_id  :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#

require 'spec_helper'

describe Patient::EmailAddress do

  context "attributes" do

    before :each do
      @email_address = Patient::EmailAddress.new
      #Personal Email code
      @email_code = Code::Email.find_by_reference("9904001")
    end

    it "has an email code" do
      @email_address.email_code = @email_code
      @email_address.save.should eq(true)
      email = Patient::EmailAddress.find(@email_address.id)
      email.email_code.description.should eq("Personal")
    end
  end
end
