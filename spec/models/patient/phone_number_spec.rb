# == Schema Information
#
# Table name: patient_phone_numbers
#
#  id              :integer          not null, primary key
#  patient_id      :integer
#  phone_code_type :string(255)
#  phone_code_id   :integer
#  phone           :string(255)
#  correlation_id  :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#

require 'spec_helper'

describe Patient::PhoneNumber do

  context "attributes" do 

    before :each do 
      @phone_number = Patient::PhoneNumber.new
      # Fax code
      @phone_code = Code::Phone.find_by_reference("9913001")
    end

    it "has a phone code" do
      @phone_number.phone_code = @phone_code
      @phone_number.save.should eq(true)

      phone = Patient::PhoneNumber.find(@phone_number.id)

      phone.phone_code.description.should eq("Fax") 
    end

  end
end
