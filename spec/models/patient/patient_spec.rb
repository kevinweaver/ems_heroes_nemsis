# == Schema Information
#
# Table name: patient_patients
#
#  id                     :integer          not null, primary key
#  trip_report_id         :integer
#  first_name             :string(255)
#  first_name_type        :string(255)
#  last_name              :string(255)
#  last_name_type         :string(255)
#  middle_name            :string(255)
#  home_address           :string(255)
#  home_address2          :string(255)
#  home_city              :string(255)
#  home_county            :string(255)
#  home_state             :string(255)
#  home_zip_code          :string(255)
#  home_zip_code_type     :string(255)
#  home_country           :string(255)
#  home_census_tract      :string(255)
#  social_security_number :integer
#  gender                 :string(255)
#  gender_type            :string(255)
#  age                    :string(255)
#  age_type               :string(255)
#  age_units_type         :string(255)
#  age_units              :string(255)
#  date_of_birth_type     :string(255)
#  date_of_birth          :string(255)
#  drivers_license_state  :string(255)
#  drivers_license_number :string(255)
#  created_at             :datetime
#  updated_at             :datetime
#

require 'spec_helper'

describe Patient::Patient do
  
  context "validates basic functionality" do 
    
    before :each do 
      @patient = FactoryGirl.create(:patient_patient)
    end
    
    it "has many races" do
      @patient.races.length.should eq(0)
        
      asian_code = Code::Race.where(:reference => '2514003').first

      race = Patient::Race.new
      race.race_code = asian_code

      race.patient = @patient 
      race.save.should eq(true)
     
      race.patient_id.should eq(@patient.id)

      patient = Patient::Patient.find(@patient.id)
      patient.races.first.race_code.description.should eq("Asian")
    end
    
    it "has many phone numbers" do
      @patient.phone_numbers.length.should eq(0)

      phone_number = Patient::PhoneNumber.new
      fax_code = Code::Phone.find_by_description("Fax")
      phone_number.phone_code = fax_code

      phone_number.patient = @patient
      phone_number.save.should eq(true)

      patient = Patient::Patient.find(@patient.id)
      patient.phone_numbers.length.should eq(1)
      patient.phone_numbers.first.phone_code.reference.should eq("9913001")
    end

    it "has many email addresses" do
      @patient.email_addresses.length.should eq(0)

      email_address = Patient::EmailAddress.new
      email_code = Code::Email.find_by_description("Personal")
      email_address.email_code = email_code

      email_address.patient = @patient
      email_address.save.should eq(true)

      patient = Patient::Patient.find(@patient.id)
      patient.email_addresses.length.should eq(1)
      patient.email_addresses.first.email_code.reference.should eq("9904001")
    end
  end
end
