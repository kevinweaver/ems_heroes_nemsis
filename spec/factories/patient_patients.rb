# == Schema Information
#
# Table name: patient_patients
#
#  id                     :integer          not null, primary key
#  trip_report_id         :integer
#  first_name             :string(255)
#  first_name_type        :string(255)
#  last_name              :string(255)
#  last_name_type         :string(255)
#  middle_name            :string(255)
#  home_address           :string(255)
#  home_address2          :string(255)
#  home_city              :string(255)
#  home_county            :string(255)
#  home_state             :string(255)
#  home_zip_code          :string(255)
#  home_zip_code_type     :string(255)
#  home_country           :string(255)
#  home_census_tract      :string(255)
#  social_security_number :integer
#  gender                 :string(255)
#  gender_type            :string(255)
#  age                    :string(255)
#  age_type               :string(255)
#  age_units_type         :string(255)
#  age_units              :string(255)
#  date_of_birth_type     :string(255)
#  date_of_birth          :string(255)
#  drivers_license_state  :string(255)
#  drivers_license_number :string(255)
#  created_at             :datetime
#  updated_at             :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :patient_patient, :class => 'Patient::Patient' do
    first_name "MyString"
    first_name_type "MyString"
    last_name_type "MyString"
    last_name "MyString"
    middle_name "MyString"
    home_address "MyString"
    home_address2 "MyString"
    home_city "MyString"
    home_county "MyString"
    home_state "MyString"
    home_zip_code "MyString"
    home_zip_code_type "MyString"
    home_country "MyString"
    home_census_tract "MyString"
    social_security_number 1
    gender "MyString"
    gender_type "MyString"
    age "MyString"
    age_type "MyString"
    age_units_type "MyString"
    age_units "MyString"
    date_of_birth_type "MyString"
    date_of_birth "MyString"
    drivers_license_state "MyString"
    drivers_license_number "MyString"
  end
end
