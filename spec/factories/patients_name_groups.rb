# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :patients_name_group, :class => 'Patients::NameGroup' do
    patient nil
    first_name "MyString"
    first_name_type "MyString"
    last_name "MyString"
    last_name_type "MyString"
    middle_name "MyString"
  end
end
