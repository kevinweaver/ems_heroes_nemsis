# == Schema Information
#
# Table name: patient_races
#
#  id             :integer          not null, primary key
#  patient_id     :integer
#  created_at     :datetime
#  updated_at     :datetime
#  race_code_type :string(255)
#  race_code_id   :integer
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :patient_race, :class => 'Patient::Race' do
    patient nil
    race_type "MyString"
    race "MyString"
  end
end
