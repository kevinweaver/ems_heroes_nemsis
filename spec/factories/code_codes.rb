# == Schema Information
#
# Table name: code_codes
#
#  id          :integer          not null, primary key
#  reference   :string(255)
#  description :string(255)
#  type        :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :code_code, :class => 'Code::Code' do
    reference "MyString"
    description "MyString"
    type ""
  end
end
