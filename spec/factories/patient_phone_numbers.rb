# == Schema Information
#
# Table name: patient_phone_numbers
#
#  id              :integer          not null, primary key
#  patient_id      :integer
#  phone_code_type :string(255)
#  phone_code_id   :integer
#  phone           :string(255)
#  correlation_id  :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :patient_phone_number, :class => 'Patient::PhoneNumber' do
    patient nil
    phone_code_type "MyString"
    phone_code_id 1
    phone "MyString"
    correlation_id "MyString"
  end
end
