# == Schema Information
#
# Table name: trip_reports
#
#  id         :integer          not null, primary key
#  run_number :integer
#  created_at :datetime
#  updated_at :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :trip_report do
    run_number 1
  end
end
