# == Schema Information
#
# Table name: patient_email_addresses
#
#  id              :integer          not null, primary key
#  patient_id      :integer
#  email_code_type :string(255)
#  email_code_id   :integer
#  email           :string(255)
#  correlation_id  :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :patient_email_address, :class => 'Patient::EmailAddress' do
    patient nil
    email_code_type "MyString"
    email_code_id 1
    email "MyString"
    correlation_id "MyString"
  end
end
