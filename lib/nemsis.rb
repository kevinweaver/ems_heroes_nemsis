module Nemsis
  module Codeable

    def self.included(base)
      base.extend ClassMethods
    end

    module ClassMethods
      def has_nemsis_code(attr)
        define_getter(attr)
        define_setter(attr)
      end

      def define_setter(attr)
        self.send(:define_method, "#{attr}_code=") do |obj|
          self.send("#{attr}_code_type=", obj.class.to_s)
          self.send("#{attr}_code_id=", obj.id)
        end
      end

      def define_getter(attr)
        self.send(:define_method, "#{attr}_code") do
          Code::Code.limit(1).where(:type => self.send("#{attr}_code_type"),
                           :id => self.send("#{attr}_code_id")).first
        end
      end
    end

  end
end
