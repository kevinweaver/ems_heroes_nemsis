require 'spec_helper'

describe "code/codes/edit" do
  before(:each) do
    @code_code = assign(:code_code, stub_model(Code::Code,
      :reference => "MyString",
      :description => "MyString",
      :type => ""
    ))
  end

  it "renders the edit code_code form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", code_code_path(@code_code), "post" do
      assert_select "input#code_code_reference[name=?]", "code_code[reference]"
      assert_select "input#code_code_description[name=?]", "code_code[description]"
      assert_select "input#code_code_type[name=?]", "code_code[type]"
    end
  end
end
