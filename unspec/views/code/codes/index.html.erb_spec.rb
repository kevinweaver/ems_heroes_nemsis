require 'spec_helper'

describe "code/codes/index" do
  before(:each) do
    assign(:code_codes, [
      stub_model(Code::Code,
        :reference => "Reference",
        :description => "Description",
        :type => "Type"
      ),
      stub_model(Code::Code,
        :reference => "Reference",
        :description => "Description",
        :type => "Type"
      )
    ])
  end

  it "renders a list of code/codes" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Reference".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => "Type".to_s, :count => 2
  end
end
