require 'spec_helper'

describe "code/codes/new" do
  before(:each) do
    assign(:code_code, stub_model(Code::Code,
      :reference => "MyString",
      :description => "MyString",
      :type => ""
    ).as_new_record)
  end

  it "renders new code_code form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", code_codes_path, "post" do
      assert_select "input#code_code_reference[name=?]", "code_code[reference]"
      assert_select "input#code_code_description[name=?]", "code_code[description]"
      assert_select "input#code_code_type[name=?]", "code_code[type]"
    end
  end
end
