require 'spec_helper'

describe "code/codes/show" do
  before(:each) do
    @code_code = assign(:code_code, stub_model(Code::Code,
      :reference => "Reference",
      :description => "Description",
      :type => "Type"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Reference/)
    rendered.should match(/Description/)
    rendered.should match(/Type/)
  end
end
