require 'spec_helper'

describe "trip_reports/show" do
  before(:each) do
    @trip_report = assign(:trip_report, stub_model(TripReport,
      :run_number => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
  end
end
