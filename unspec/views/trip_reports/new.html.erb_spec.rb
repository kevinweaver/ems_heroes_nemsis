require 'spec_helper'

describe "trip_reports/new" do
  before(:each) do
    assign(:trip_report, stub_model(TripReport,
      :run_number => 1
    ).as_new_record)
  end

  it "renders new trip_report form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", trip_reports_path, "post" do
      assert_select "input#trip_report_run_number[name=?]", "trip_report[run_number]"
    end
  end
end
