require 'spec_helper'

describe "trip_reports/index" do
  before(:each) do
    assign(:trip_reports, [
      stub_model(TripReport,
        :run_number => 1
      ),
      stub_model(TripReport,
        :run_number => 1
      )
    ])
  end

  it "renders a list of trip_reports" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end
