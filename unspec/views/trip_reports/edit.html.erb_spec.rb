require 'spec_helper'

describe "trip_reports/edit" do
  before(:each) do
    @trip_report = assign(:trip_report, stub_model(TripReport,
      :run_number => 1
    ))
  end

  it "renders the edit trip_report form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", trip_report_path(@trip_report), "post" do
      assert_select "input#trip_report_run_number[name=?]", "trip_report[run_number]"
    end
  end
end
