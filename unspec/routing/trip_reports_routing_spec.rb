require "spec_helper"

describe TripReportsController do
  describe "routing" do

    it "routes to #index" do
      get("/trip_reports").should route_to("trip_reports#index")
    end

    it "routes to #new" do
      get("/trip_reports/new").should route_to("trip_reports#new")
    end

    it "routes to #show" do
      get("/trip_reports/1").should route_to("trip_reports#show", :id => "1")
    end

    it "routes to #edit" do
      get("/trip_reports/1/edit").should route_to("trip_reports#edit", :id => "1")
    end

    it "routes to #create" do
      post("/trip_reports").should route_to("trip_reports#create")
    end

    it "routes to #update" do
      put("/trip_reports/1").should route_to("trip_reports#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/trip_reports/1").should route_to("trip_reports#destroy", :id => "1")
    end

  end
end
