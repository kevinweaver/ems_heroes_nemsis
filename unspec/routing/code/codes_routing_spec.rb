require "spec_helper"

describe Code::CodesController do
  describe "routing" do

    it "routes to #index" do
      get("/code/codes").should route_to("code/codes#index")
    end

    it "routes to #new" do
      get("/code/codes/new").should route_to("code/codes#new")
    end

    it "routes to #show" do
      get("/code/codes/1").should route_to("code/codes#show", :id => "1")
    end

    it "routes to #edit" do
      get("/code/codes/1/edit").should route_to("code/codes#edit", :id => "1")
    end

    it "routes to #create" do
      post("/code/codes").should route_to("code/codes#create")
    end

    it "routes to #update" do
      put("/code/codes/1").should route_to("code/codes#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/code/codes/1").should route_to("code/codes#destroy", :id => "1")
    end

  end
end
