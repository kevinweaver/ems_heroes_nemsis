# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20130410011556) do

  create_table "code_codes", force: true do |t|
    t.string   "reference"
    t.string   "description"
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "patient_email_addresses", force: true do |t|
    t.integer  "patient_id"
    t.string   "email_code_type"
    t.integer  "email_code_id"
    t.string   "email"
    t.string   "correlation_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "patient_email_addresses", ["patient_id"], name: "index_patient_email_addresses_on_patient_id"

  create_table "patient_patients", force: true do |t|
    t.integer  "trip_report_id"
    t.string   "first_name"
    t.string   "first_name_type"
    t.string   "last_name"
    t.string   "last_name_type"
    t.string   "middle_name"
    t.string   "home_address"
    t.string   "home_address2"
    t.string   "home_city"
    t.string   "home_county"
    t.string   "home_state"
    t.string   "home_zip_code"
    t.string   "home_zip_code_type"
    t.string   "home_country"
    t.string   "home_census_tract"
    t.integer  "social_security_number"
    t.string   "gender"
    t.string   "gender_type"
    t.string   "age"
    t.string   "age_type"
    t.string   "age_units_type"
    t.string   "age_units"
    t.string   "date_of_birth_type"
    t.string   "date_of_birth"
    t.string   "drivers_license_state"
    t.string   "drivers_license_number"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "patient_patients", ["trip_report_id"], name: "index_patient_patients_on_trip_report_id"

  create_table "patient_phone_numbers", force: true do |t|
    t.integer  "patient_id"
    t.string   "phone_code_type"
    t.integer  "phone_code_id"
    t.string   "phone"
    t.string   "correlation_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "patient_phone_numbers", ["patient_id"], name: "index_patient_phone_numbers_on_patient_id"

  create_table "patient_races", force: true do |t|
    t.integer  "patient_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "race_code_type"
    t.integer  "race_code_id"
  end

  add_index "patient_races", ["patient_id"], name: "index_patient_races_on_patient_id"

  create_table "trip_reports", force: true do |t|
    t.integer  "run_number"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
