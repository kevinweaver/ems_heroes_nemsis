# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#

# Negative Value Codes
nv_codes_array = [
  ['7701001', 'Not Applicable'],
  ['7701003', 'Not Recorded'],
  ['7701005', 'Not Reporting']
]

nv_codes_array.each do |code|
  Code::NotValue.create(reference: code[0], description: code[1])
end

# Pertinent Negative Codes 
pn_codes_array = [
  ['8801019', 'Refused'],
  ['8801023', 'Unable to Complete']
]

pn_codes_array.each do |code|
  Code::PertinentNegative.create(reference: code[0], description: code[1])
end

# Race Codes
race_codes_array = [ 
  ['2514001',	'American Indian or Alaska Native'],
  ['2514003',	'Asian'],
  ['2514005', 'Black or African American'],
  ['2514007',	'Hispanic or Latino'],
  ['2514009',	'Native Hawaiian or Other Pacific Islander'],
  ['2514011', 'White']
]

race_codes_array.each do |code|
  Code::Race.create(reference: code[0], description: code[1])
end

#Phone Codes
phone_codes_array = [
  ['9913001', 'Fax'],
  ['9913003', 'Home'],
  ['9913005', 'Mobile'],
  ['9913007', 'Pager'],
  ['9913009', 'Work']
]

phone_codes_array.each do |code|
  Code::Phone.create(reference: code[0], description: code[1])
end

#Email Codes
email_codes_array = [
  ['9904001', 'Personal'],
  ['9904003', 'Work']
]

email_codes_array.each do |code|
  Code::Email.create(reference: code[0], description: code[1])
end
