class CreateCodeCodes < ActiveRecord::Migration
  def change
    create_table :code_codes do |t|
      t.string :reference
      t.string :description
      t.string :type

      t.timestamps
    end
  end
end
