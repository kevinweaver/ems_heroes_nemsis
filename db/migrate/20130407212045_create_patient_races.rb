class CreatePatientRaces < ActiveRecord::Migration
  def change
    create_table :patient_races do |t|
      t.references :patient, index: true
      t.string :race_type
      t.string :race

      t.timestamps
    end
  end
end
