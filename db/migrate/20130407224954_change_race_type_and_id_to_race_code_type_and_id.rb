class ChangeRaceTypeAndIdToRaceCodeTypeAndId < ActiveRecord::Migration
  def change
    remove_column :patient_races, :race_type
    remove_column :patient_races, :race_id
    add_column :patient_races, :race_code_type, :string
    add_column :patient_races, :race_code_id, :integer
  end
end
