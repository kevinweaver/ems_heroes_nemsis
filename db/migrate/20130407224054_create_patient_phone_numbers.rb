class CreatePatientPhoneNumbers < ActiveRecord::Migration
  def change
    create_table :patient_phone_numbers do |t|
      t.references :patient, index: true
      t.string :phone_code_type
      t.integer :phone_code_id
      t.string :phone
      t.string :correlation_id

      t.timestamps
    end
  end
end
