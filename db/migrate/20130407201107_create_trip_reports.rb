class CreateTripReports < ActiveRecord::Migration
  def change
    create_table :trip_reports do |t|
      t.integer :run_number

      t.timestamps
    end
  end
end
