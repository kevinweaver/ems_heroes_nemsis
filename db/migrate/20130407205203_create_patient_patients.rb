class CreatePatientPatients < ActiveRecord::Migration
  def change
    create_table :patient_patients do |t|
      t.references :trip_report, index: true
      t.string :first_name
      t.string :first_name_type
      t.string :last_name
      t.string :last_name_type
      t.string :middle_name
      t.string :home_address
      t.string :home_address2
      t.string :home_city
      t.string :home_county
      t.string :home_state
      t.string :home_zip_code
      t.string :home_zip_code_type
      t.string :home_country
      t.string :home_census_tract
      t.integer :social_security_number
      t.string :gender
      t.string :gender_type
      t.string :age
      t.string :age_type
      t.string :age_units_type
      t.string :age_units
      t.string :date_of_birth_type
      t.string :date_of_birth
      t.string :drivers_license_state
      t.string :drivers_license_number

      t.timestamps
    end
  end
end
