class RemoveRaceAndAddRaceIdToRace < ActiveRecord::Migration
  def change
    remove_column :patient_races, :race
    add_column :patient_races, :race_id, :integer
  end
end
