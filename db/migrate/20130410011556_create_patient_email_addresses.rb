class CreatePatientEmailAddresses < ActiveRecord::Migration
  def change
    create_table :patient_email_addresses do |t|
      t.references :patient, index: true
      t.string :email_code_type
      t.integer :email_code_id
      t.string :email
      t.string :correlation_id

      t.timestamps
    end
  end
end
